package test;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Before;
import org.junit.Test;

import model.Display;
import model.TextDisplay;

public class TestTextDisplay {

	private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

	@Before
	public void setUpStream() {
		System.setOut(new PrintStream(outContent));

	}

	@Test
	public void test() {
		Display textdisplay = new TextDisplay();
		String testout="Hello World";
		textdisplay.show(testout);
		assertEquals(testout +"\n", outContent.toString());
	}

}
