package test;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

import model.FileProvider;

public class TestFileProvider {

	@Test(expected = FileNotFoundException.class)
	public void testNotExistingFile() throws FileNotFoundException{
		FileProvider fileprovider = new FileProvider();
		String totest  = fileprovider.get("nonexistingfile.txt");
	}

	@Test(expected = IllegalArgumentException.class)
	public void testIllegalArgument() throws FileNotFoundException {
		FileProvider fileProvider = new FileProvider();
		
		String totest= fileProvider.get(null);
	}
	
	@Test
	public void testWithTestFile() {
		FileProvider fileprovider = new FileProvider();
		String totest = null;
		try {
			totest = fileprovider.get("src/test/test.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals(totest, "test\ntest\n");
	}

}
