package model;

public class FileBrowser extends Browser {
	 /**
     * @param d The display to be used for this browser
     * @param f The file to read content from
     */
	FileBrowser(Display d, ContentProvider p) {
		super(d, p);
	}

}
