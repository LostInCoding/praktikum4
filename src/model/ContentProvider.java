package model;

import java.io.FileNotFoundException;

public abstract class ContentProvider {
public abstract String get(String req) throws FileNotFoundException;
}
