package model;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;

public class FileProvider extends ContentProvider {
	 /**
     * Returns the contents from the given file.
     *
     * @param filename file to open
     * @return String content of file
     */
	@Override
	public String get(String req) throws FileNotFoundException{
		if(req==null) {
			throw new IllegalArgumentException("String was null");
		}
		String line;
		File file = new File(req);
		if(!file.exists()) {
			throw new FileNotFoundException("");
		}
		LineNumberReader reader = null;
		StringBuilder sb = null;
		try {
			reader = new LineNumberReader(new FileReader(req));
			sb = new StringBuilder();
			while ((line = reader.readLine()) != null) {
				sb.append(line);
				sb.append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return sb.toString();
	}

	
	public static void main(String[] args) {
		Browser browser = new FileBrowser(new TextDisplay(),new FileProvider());
		if(args[0]!=null) {
			browser.present(args[0]);	
		}else {
			throw new IllegalArgumentException("Es muss eine Datei angegeben werden");
		}
		
	}
	
}
