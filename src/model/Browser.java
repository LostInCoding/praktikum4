package model;

import java.io.FileNotFoundException;

abstract class Browser {

    private final Display display;
    private final ContentProvider provider;

    /**
     * @param d The display to be used for this browser
     * @param p The content provider to be used
     */
    Browser(Display d, ContentProvider p) {
        this.display = d;
        this.provider = p;
    }

    /**
     * Gets content from the provider and presents it on the display.
     *
     * @param resource the text to present on display
     */
    void present(String resource) {
    	try{
    		String msg = this.provider.get(resource);	
    		this.display.show(msg);
    	}catch (FileNotFoundException e) {
    		e.printStackTrace();
    	}
        
        
    }
}