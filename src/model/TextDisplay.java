package model;

public class TextDisplay implements Display {
	 /**
     * Prints message to stdout
     *
     * @param msg message to display
     */
	public void show(String msg) {
		System.out.println(msg);

	}

}
